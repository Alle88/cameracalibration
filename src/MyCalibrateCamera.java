
import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Year;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import Jama.EigenvalueDecomposition;
import Jama.Matrix;

/**
 * Determine the camera matrix from several images of pattern
 * @author Alle
 */
public class MyCalibrateCamera {	
	
	private static Matrix A;
	private static Matrix V;
	private static List<Matrix> P_list; 	
	private static Mat KameraMatrix;
	private static List<Mat> tMatList = new ArrayList<Mat>();
	private static List<Mat> rMatList = new ArrayList<Mat>();
	private static List<Mat> imagePointsCorrected = new ArrayList<Mat>();
	
	public static double calibrateCamera(List<Mat> objectPoints3f, List<Mat> imagePoints2f,
			ArrayList<Mat> imageMatList, Mat cameraMatrix, Mat distCoeffs, List<Mat> rvecs,
			List<Mat> tvecs) {		
		
		int numberOfImages = imageMatList.size();		
		int pontMegfSzam = objectPoints3f.get(0).height();		
		double rms = 0;
		
		V = new Matrix(numberOfImages*2, 6);		
		P_list = new ArrayList<Matrix>();		
		tMatList = tvecs;
		rMatList = rvecs;
		
		//Képenkénti homográfia becslése:		
		for (int i = 0; i < numberOfImages; i++) {			
			
			A = new Matrix(pontMegfSzam*2, 9);	
			Matrix AT_A = new Matrix(pontMegfSzam*2, pontMegfSzam*2);
			
			A = calculate_A_matrix(i, pontMegfSzam, objectPoints3f, imagePoints2f);		
			AT_A = A.transpose().times(A);					
			
			EigenvalueDecomposition eigen = new EigenvalueDecomposition(AT_A);
			double[] eigenValues = eigen.getRealEigenvalues();			
			Matrix eigenVectors = eigen.getV();				
			
			Matrix P = MatOperations.FillJamaMatrixWithEigenVectors(eigenVectors, getMinValuesIndex(eigenValues));	
			rms += AlgebraError(A, P)*AlgebraError(A, P);				
			//MatOperations.LoggerHelper(P, i + ". Homográfia");	
			
			P_list.add(P);
		} 
		
		//Kamera mátrix belső paramétereinek becslése a homográfiák alapján:		
		calculate_V_matrix(numberOfImages);
			
		Matrix VT_V = new Matrix(numberOfImages*2, numberOfImages*2);
		VT_V = V.transpose().times(V);				
			
		EigenvalueDecomposition eigen = new EigenvalueDecomposition(VT_V);
		double[] eigenValues2 = eigen.getRealEigenvalues();			
		Matrix eigenVectors2 = eigen.getV();			
			
		Mat B = FillBMatrixWithEigenVectorsInColumnOrder(eigenVectors2, getMinValuesIndex(eigenValues2));
		//MatOperations.LoggerHelper(B, "B");
		FillKameraMatrix(B);
		
		MatOperations.LoggerHelper(KameraMatrix, "Kamera");
		KameraMatrix.copyTo(cameraMatrix);		
		
		calculateDistorsionFreePoints(objectPoints3f, imagePoints2f, numberOfImages);		
		calculate_D_matrix(pontMegfSzam, imagePoints2f);
		
		calculateExtrinsicParams(rvecs, tvecs);	
		tvecs = tMatList;
		rvecs = rMatList;
				
		return (Math.sqrt(rms))/numberOfImages;		
	}		
	
	private static void calculateDistorsionFreePoints(List<Mat> objectPoints3f, List<Mat> imagePoints2f, int numberOfImages) {
	
		for (int k = 0; k < numberOfImages; k++) { //képek 				
		
		Matrix uv1 = new Matrix(3, 1);
		Matrix uv1_real = new Matrix(3, 1);
		Matrix distVec = new Matrix(5, 1);
		
			Mat oneImgPoints = objectPoints3f.get(k);
			Matrix oneImgProj = P_list.get(k);
			Mat realPointsMat = new Mat(oneImgPoints.rows(), 3, CvType.CV_64F);
			
			for (int p = 0; p < oneImgPoints.rows(); p++) { //képpontok
				
				uv1.set(0, 0, oneImgPoints.get(p, 0)[0]);
				uv1.set(1, 0, oneImgPoints.get(p, 0)[1]);
				uv1.set(2, 0, 1.0);			
				
				uv1_real = oneImgProj.times(uv1);
				uv1_real = uv1_real.times(1.0/uv1_real.get(2, 0));	 //homogén osztás				
				realPointsMat.put(p, 0, new double[]{uv1_real.get(0, 0), uv1_real.get(1, 0), uv1_real.get(2, 0)});
			}
			imagePointsCorrected.add(realPointsMat);
		}			
	}	

	//Nem teljesen szép az eredménye:
	private static void calculateExtrinsicParams(List<Mat> rvecs, List<Mat> tvecs) {
		///TODO lambda		
		//double lambda = 0.8539998617016514; 
		
		for (int i = 0; i < P_list.size(); i++) {			
			Matrix P = P_list.get(i);
			Matrix h1 = MatOperations.getOneColFromMatrix(P, 0);
			Matrix h2 = MatOperations.getOneColFromMatrix(P, 1);
			Matrix h3 = MatOperations.getOneColFromMatrix(P, 2);
			Matrix r1 = P.inverse().times(h1);
			Matrix r2 = P.inverse().times(h2);
			Matrix r3 = r1.arrayTimes(r2);
			Matrix t = P.inverse().times(h3);
			
			tMatList.add(MatOperations.convertJama2OCV(t));
			rMatList.add(MatOperations.convertFromArraysToOneMatrix(r1, r2, r3));
		}			
	}
	
	private static Matrix calculate_A_matrix(int imgNr, int pontMegfSzam, List<Mat> objectPoints3f, List<Mat> imagePoints2f) {
		Matrix Amx = new Matrix(pontMegfSzam*2, 9);	
		double[] data = new double[pontMegfSzam * 2 * 12];		
		Mat objPoints = objectPoints3f.get(imgNr);
		Mat imgPoints = imagePoints2f.get(imgNr);		
		
		int i = 0;
		for (int j = 0; j < pontMegfSzam; j++) {				
			double x = objPoints.get(j, 0)[0];				
			double y = objPoints.get(j, 0)[1];				
			double u = imgPoints.get(j, 0)[0];
			double v = imgPoints.get(j, 0)[1];				

			data[i++] = x; 	data[i++] = y; 	data[i++] = 1d; 
			data[i++] = 0d; data[i++] = 0d; data[i++] = 0d; data[i++] = -u * x; 
			data[i++] = -u * y; data[i++] = -u;

			data[i++] = 0d; data[i++] = 0d; data[i++] = 0d; 
			data[i++] = x; data[i++] = y; data[i++] = 1d; data[i++] = -v * y; 
			data[i++] = -v * y; data[i++] = -v;
		}
		
		Amx = MatOperations.fillJamaMatrix(Amx, data);	
		return Amx;
	}
	
	private static void calculate_D_matrix(int pontMegfSzam, List<Mat> imagePoints2f) {

		double fx = KameraMatrix.get(0, 0)[0];
		double fy = KameraMatrix.get(1, 1)[0];
		double u0 = KameraMatrix.get(0, 2)[0];
		double v0 = KameraMatrix.get(1, 2)[0];
		// System.out.println(fx + ", " + fy + "  " +u0 + ", " + v0);
		for (int img = 0; img < imagePoints2f.size(); img++) {

			double[] dataD = new double[pontMegfSzam * 5 * 4];
			double[] dataDvec = new double[pontMegfSzam * 4];

			Mat correctedPts = imagePointsCorrected.get(img);
			Mat imgPts = imagePoints2f.get(img);

			int i = 0;
			int m = 0;
			for (int j = 0; j < correctedPts.rows(); j++) {

				double u = correctedPts.get(j, 0)[0];
				double v = correctedPts.get(j, 1)[0];
				double u_ = imgPts.get(0, 0)[0];
				double v_ = imgPts.get(0, 0)[1];
				double x = (u - u0);
				double y = (v - v0);

				dataD[i++] = (u - u0) * (x * x + y * y);
				dataD[i++] = (u - u0) * ((x * x + y * y) * (x * x + y * y));
				dataD[i++] = 0d;
				dataD[i++] = 0d;
				dataD[i++] = (u - u0)
						* ((x * x + y * y) * (x * x + y * y) * (x * x + y * y) * (x * x + y * y));

				dataD[i++] = (v - v0) * (x * x + y * y);
				dataD[i++] = (v - v0) * ((x * x + y * y) * (x * x + y * y));
				dataD[i++] = 0d;
				dataD[i++] = 0d;
				dataD[i++] = (v - v0) * ((x * x + y * y) * (x * x + y * y) * (x * x + y * y) * (x * x + y * y));

				dataD[i++] = 0d;
				dataD[i++] = 0d;
				dataD[i++] = fx * 2 * x * y;
				dataD[i++] = fx * ((x * x + y * y) + 2 * x * x);
				dataD[i++] = 0d;

				dataD[i++] = 0d;
				dataD[i++] = 0d;
				dataD[i++] = fy * ((x * x + y * y) + 2 * y * y);
				dataD[i++] = fy * 2 * x * y;
				dataD[i++] = 0d;

				// d vector
				dataDvec[m++] = u_ - u;
				dataDvec[m++] = v_ - v;
				dataDvec[m++] = u_ - u;
				dataDvec[m++] = v_ - v;
			}

			Matrix D = new Matrix(pontMegfSzam * 2, 5);
			Matrix DT_D = new Matrix(5, 5);
			Matrix DT_D_inv = new Matrix(5, 5);
			Matrix d_Vec = new Matrix(pontMegfSzam * 2, 1);
			Matrix k = new Matrix(5, 1);

			D = MatOperations.fillJamaMatrix(D, dataD);
			//MatOperations.LoggerHelper(D, "D");
			DT_D = (D.transpose()).times(D);
			//MatOperations.LoggerHelper(DT_D, "DtD");
			DT_D_inv = DT_D.inverse();
			//MatOperations.LoggerHelper(DT_D_inv, "DtD^-1");

			d_Vec = MatOperations.fillJamaMatrix(d_Vec, dataDvec);
			k = (DT_D_inv.times(D.transpose())).times(d_Vec);

			MatOperations.LoggerHelper(k, "k");
		}

	}
	
	private static void FillKameraMatrix(Mat B) {		
		double b11 = B.get(0, 0)[0];
		double b12 = B.get(0, 1)[0];
		double b13 = B.get(0, 2)[0];
		double b22 = B.get(1, 1)[0];
		double b23 = B.get(1, 2)[0];
		double b33 = B.get(2, 2)[0];	
		
		/**Zhang féle: */
		double v0 = ((b12*b13)-(b11*b23)) / (b11*b22 - (b12*b12) );
		double lambda = b33 - ((b13*b13) + v0*(b12*b13-b11*b23)) /b11;		
		double alfa = Math.sqrt(( lambda / b11));
		double beta = Math.sqrt( (lambda*b11 / (b11*b22 - (b12*b12)) ) );
		double gamma= -b12*(alfa*alfa)*beta / lambda;
		double u0 = gamma*v0 / beta-b13*(alfa*alfa)/lambda;		
		
		/** Step-by-step féle: */
		/*double v0 = (b12*b13-b11*b23) / (b13*b22 - (b12*b12) );
		double lambda = b33 - ((b12*b12) + v0*(b12*b13-b11*b23)) /b11;
		double alfa = Math.sqrt(( lambda / b11));		
		double beta = Math.sqrt( ( lambda*b11 / (b11*b22 - (b12*b12)) ) );
		double gamma= -b12*(alfa*alfa)*beta / lambda;
		double u0 = (gamma*v0 / beta) - b13*(alfa*alfa)/lambda;		*/		
		
		KameraMatrix = new Mat(3, 3, CvType.CV_64F);
		KameraMatrix.put(0, 0, alfa);
		KameraMatrix.put(0, 1, 0.0);
		KameraMatrix.put(0, 2, u0);
		KameraMatrix.put(1, 0, 0.0d);
		KameraMatrix.put(1, 1, beta);
		KameraMatrix.put(1, 2, v0);
		KameraMatrix.put(2, 0, 0.0d);
		KameraMatrix.put(2, 1, 0.0d);
		KameraMatrix.put(2, 2, 1.0d);			
	}

	private static void calculate_V_matrix(int kepszam) {

		for (int k = 0; k < kepszam; k++) {

			double[] firstrow = calcVij(k, 0, 1);
			double[] secondrow = MatOperations.vectorSub(calcVij(k, 0, 0), calcVij(k, 1, 1));
			
			// Step By Step cikk alapján:
			// double[] firstrow = calcV1(k));
			// double[] secondrow = calcV2(k));

			for (int j = 0; j < V.getColumnDimension(); j++) {
				V.set(2 * k, j, firstrow[j]);
			}
			for (int j = 0; j < V.getColumnDimension(); j++) {
				V.set(2 * k + 1, j, secondrow[j]);
			}
		}
	}

	private static double[] calcVij(int imgNr, int i, int j) {
		double[] V_vec = new double[6];
		
		double xi = P_list.get(imgNr).get(0, i);
		double xj = P_list.get(imgNr).get(0, j);
		double yi = P_list.get(imgNr).get(1, i);
		double yj = P_list.get(imgNr).get(1, j);
		double zi = P_list.get(imgNr).get(2, i);
		double zj = P_list.get(imgNr).get(2, j);		

		/** Zhang cikk: */
		/*V_vec[0] = xi * xj;
		V_vec[1] = xi * yj + yi * xj;
		V_vec[2] = yi * yj;
		V_vec[3] = zi * xj + xi * zj;
		V_vec[4] = zi * yj + yi * zj;
		V_vec[5] = zi * zj;	*/	
		
		/** Zhang's Camera Calibration - Step By Step cikk*/
		V_vec[0] = xi * xj;
		V_vec[1] = xj * yi + xi * xj;
		V_vec[2] = yi * yj;
		V_vec[3] = xi*zj + xj*zi;
		V_vec[4] = zi*zj + yi*zj;
		V_vec[5] = zi * zj;		
		
		
		return V_vec;
	}
	
	//Másik jegyzetben talált számítások:
	private static double[] calcV2(int imgNr) {
		double[] V_vec = new double[6];
		
		double p0 = P_list.get(imgNr).get(0, 0);
		double p1 = P_list.get(imgNr).get(0, 1);
		double p2 = P_list.get(imgNr).get(0, 2);
		double p3 = P_list.get(imgNr).get(1, 0);
		double p4 = P_list.get(imgNr).get(1, 1);
		double p5 = P_list.get(imgNr).get(1, 2);
		double p6 = P_list.get(imgNr).get(2, 0);
		double p7 = P_list.get(imgNr).get(2, 1);
		double p8 = P_list.get(imgNr).get(2, 2);
		
		V_vec[0] = (p0*p0)-(p1*p1);
		V_vec[1] = 2*(p0*p1 - p3*p1);
		V_vec[2] = (p3*p3)-(p4*p4);
		V_vec[3] = 2*(p0*p7 - p6*p1);
		V_vec[4] = 2*(p3*p7 - p6*p7);
		V_vec[5] = (p6*p6)-(p7*p7);
		
		return V_vec;
	}
	
	//Másik jegyzetben talált számítások:
	private static double[] calcV1(int imgNr) {
		double[] V_vec = new double[6];
		
		double p0 = P_list.get(imgNr).get(0, 0);
		double p1 = P_list.get(imgNr).get(0, 1);
		double p2 = P_list.get(imgNr).get(0, 2);
		double p3 = P_list.get(imgNr).get(1, 0);
		double p4 = P_list.get(imgNr).get(1, 1);
		double p5 = P_list.get(imgNr).get(1, 2);
		double p6 = P_list.get(imgNr).get(2, 0);
		double p7 = P_list.get(imgNr).get(2, 1);
		double p8 = P_list.get(imgNr).get(2, 2);
		
		V_vec[0] = p0*p1;
		V_vec[1] = (p0*p1+p3*p1);
		V_vec[2] = p3*p4;
		V_vec[3] = (p0*p7+p6*p1);
		V_vec[4] = (p3*p7+p6*p7);
		V_vec[5] = (p6*p7);
		
		return V_vec;
	}
	
	
	private static Mat FillBMatrixWithEigenVectorsInColumnOrder( Matrix eigenVectors, int minSE) {
		Mat ocvMatrix = new Mat(3, 3, CvType.CV_64F);		
		
		double data[] = new double[9];		
		data[0] = eigenVectors.get(0, minSE);	
		data[1] = eigenVectors.get(1, minSE);	
		data[2] = eigenVectors.get(3, minSE);	
		data[3] = eigenVectors.get(1, minSE);	
		data[4] = eigenVectors.get(2, minSE);	
		data[5] = eigenVectors.get(4, minSE);	
		data[6] = eigenVectors.get(3, minSE);	
		data[7] = eigenVectors.get(4, minSE);	
		data[8] = eigenVectors.get(5, minSE);
			
		ocvMatrix.put(0, 0, data);
		return ocvMatrix;
	}
	
	/** Get the least eigenvalue*/
	private static int getMinValuesIndex(double[] eigenValues){
		int minindex = 0;  //Másra inicializálni
		double min = Double.MAX_VALUE;
		for (int i = 0; i < eigenValues.length; i++) {			
			if (eigenValues[i] < min && eigenValues[i] > 0.0d) {				
				min = eigenValues[i];
				minindex = i;
			}			
		}
		//System.out.println("\n" + minindex + ".sérték: " + min + ".");
		return minindex;
	}	
	
	/** Returns the error of algebra */
	public static double AlgebraError(Matrix A, Matrix P) {	
		Matrix P_vector = MatOperations.convertColumnMatrix(P);		
		return A.times(P_vector).norm2();
	}
}
