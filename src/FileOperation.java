
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Mat;

public class FileOperation {

	public static final String DIR_NAME = "ChessBoardDetection";	

	public static List<File> listFilesInWorkingDirectory(String path) {
		File folder = new File(path);
		List<File> filesList = new ArrayList<File>();
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isFile())
				filesList.add(fileEntry);
		}
		return filesList;
	}

	public static void writeToNewTextFile(String fileName, String message) {
		try {
			FileOutputStream out = new FileOutputStream(fileName);
			out.write(message.getBytes());
			out.flush();
			out.close();
			System.out.println(fileName + " created.");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
	
	public static String listToString(List<Mat> rvecs) {
		String toString = "";

		for (int i = 0; i < rvecs.size(); i++) {

			toString += rvecs.get(i).dump() + "\n";
		}
		return toString;
	}
	
}
