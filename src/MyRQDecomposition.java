import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;

import Jama.Matrix;


public class MyRQDecomposition {
	final static double EPSILON = 1E-10;

	//Matrix Pjama = new Matrix(3, 3);	
	
	Mat Peredeti = new Mat(3, 3, CvType.CV_64F);
	Mat P = new Mat(3, 3, CvType.CV_64F);	
	Mat Qx = new Mat(3, 3, CvType.CV_64F); 
	Mat Qy = new Mat(3, 3, CvType.CV_64F);
	Mat Qz = new Mat(3, 3, CvType.CV_64F);	
	Mat Q = new Mat(3, 3, CvType.CV_64F);	
	Mat Q1 = new Mat(3, 3, CvType.CV_64F);	
	Mat Q2 = new Mat(3, 3, CvType.CV_64F);	
	Mat I = new Mat(3, 3, CvType.CV_64F);	
	
	public MyRQDecomposition(Mat p_3x4) {		
		
		p_3x4.copyTo(Peredeti);
		p_3x4.copyTo(P);					
		
		I.put(0, 0, new double[]{1, 0, 0, 
								 0, 1, 0,
								 0, 0, 1});
	}		
	
	
	public void decomp() {		

		//MatLoggerHelper(Peredeti, "Peredeti");	
		
		initQx();			
		Core.gemm(Peredeti, Qx, 1, I, 0, P); 		
		//MatLoggerHelper(P, "P*Qx");	

		initQy();		
		Core.gemm(P, Qy, 1, I, 0, P); 		
		//MatLoggerHelper(P, "P*Qx*Qy");

		initQz();		
		Core.gemm(P, Qz, 1, I, 0, P); 
		normalizeMatrix(P);
		MatLoggerHelper(P, "P*Qx*Qy*Qz");
		
		//Megszorozva Qt-vel:
		initQ();
		Core.gemm(Peredeti, Q.t(), 1, I, 0, P); 
		normalizeMatrix(P);
		MatLoggerHelper(P, "P*QT");
		leosztP33mal();
	}	
	
	private void leosztP33mal(){
		double p33 = P.get(2, 2)[0];
		
		for (int i = 0; i < P.rows(); i++) {
			for (int j = 0; j < P.cols(); j++) {					
					P.put(i, j, P.get(i, j)[0] / p33 );	
				}
		}
		
		MatLoggerHelper(P, "P leosztva P33-mal");
	}
	
	private void initQ() {		
		Core.gemm( Qy, Qx, 1, new Mat(), 0, P, Core.GEMM_1_T + Core.GEMM_2_T);
		Core.gemm( Qz, P, 1, new Mat(), 0, Q, Core.GEMM_1_T);
		
		MatLoggerHelper(Q, "QzT*QyT*QxT");
	}
	
	
	private void initQx() {			
		double alfaX2 = Math.atan2(-Peredeti.get(2, 1)[0], Peredeti.get(2, 2)[0]);		
		System.out.println("alfaX " + alfaX2);		
		
		double cos = Math.cos(alfaX2);
		double sin = Math.sin(alfaX2);
		double data[] = {1,		0,		0, 
						 0, 	cos,	-sin,
						 0, 	sin, 	cos};
		Qx.put(0, 0, data);			
	}	
	
	private void initQy() {		
		double alfaY2 = Math.atan2(P.get(2, 0)[0], P.get(2, 2)[0]);
		System.out.println("alfaY " + alfaY2);
		double cos = Math.cos(alfaY2);
		double sin = Math.sin(alfaY2);
		double data[] = {cos,	0,		sin, 
						 0, 	1,		0,
						 -sin, 	0, 		cos};
		Qy.put(0, 0, data);		
	}	

	private void initQz() {		
		double alfaZ2 = Math.atan2(-P.get(1, 0)[0], P.get(1, 1)[0]);
		System.out.println("alfaZ " + alfaZ2);
		double cos = Math.cos(alfaZ2);
		double sin = Math.sin(alfaZ2);
		double data[] = {cos,	-sin,	0, 
						 sin, 	cos,	0,
						 0, 	0, 		1};
		Qz.put(0, 0, data);			
	}	

	private static void MatLoggerHelper(Mat a, String s ) {
		System.out.println( "\n'" + s + "' mátrix: " + a.height() + "x" +a.width() + "-es lett:");		
		String mx = "\n";
		for (int i = 0; i < a.height(); i++) {
			mx += "[";
			for (int j = 0; j < a.width(); j++) {
				mx += a.get(i, j)[0] + ",\t";
			}
			mx += "]\r\n";
		}
		System.out.println(mx );		
	}	
	
	private static Mat normalizeMatrix(Mat m) {		
		for (int i = 0; i < m.rows(); i++) {
			for (int j = 0; j < m.cols(); j++) {	
				if (Math.abs(m.get(i, j)[0]) < EPSILON) {
					m.put(i, j, 0.0);	
				}					
				
			}			
		}
		return m;		
	}
}
