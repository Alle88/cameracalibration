import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.opencv.calib3d.Calib3d;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.MatOfPoint3f;
import org.opencv.core.Point;
import org.opencv.core.Point3;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

public class MainTest {
	static { 
		 System.loadLibrary(Core.NATIVE_LIBRARY_NAME); 
	 }
	public static Size PATTERN_SIZE = new Size(3,3);
	public static int CALIB_PARAMS = 0;		
	private static List objectPoints;
	private static List imagePoints;
	private static ArrayList<Mat> imageMatList;
	private static MatOfPoint3f mCornersInChessSpace3f;
	static Mat cameraMatrix = Mat.eye(3, 3, CvType.CV_64F);
	static Mat distCoeffs = Mat.zeros(5, 1, CvType.CV_64F); //lens distortion parameters
	static List<Mat> rvecs = new ArrayList<Mat>(); //Output vector of rotation vectors (see "Rodrigues") estimated for each pattern view. That is, each k-th rotation vector together with the corresponding k-th translation vector (see the next output parameter description) brings the calibration pattern from the model coordinate space (in which object points are specified) to the world coordinate space, that is, a real position of the calibration pattern in the k-th pattern view (k=0.. *M* -1).
	static List<Mat> tvecs = new ArrayList<Mat>(); //tvecs Output vector of translation vectors estimated for each pattern view.
	static int flagsCalib = Calib3d.CALIB_ZERO_TANGENT_DIST | Calib3d.CALIB_FIX_PRINCIPAL_POINT | Calib3d.CALIB_FIX_K4 | Calib3d.CALIB_FIX_K5;
	
	private static String toFileString = "";
	 
	public static void main(String[] args) {
		
		objectPoints = new ArrayList<MatOfPoint3f>();
		imagePoints = new ArrayList<MatOfPoint2f>();
		imageMatList = new ArrayList<Mat>();
		mCornersInChessSpace3f = getCorner3f();		
		calibrateCameraFromImages(FileOperation.DIR_NAME);	
		
	}	
	
	//Camera calibration képekből
	public static void calibrateCameraFromImages(String pathStorage) {	
			
			getAllCornersFromImages(pathStorage);		
			
			// OpenCV kamera kalibrálás:			
			/*double errReproj1 = Calib3d.calibrateCamera(objectPoints, imagePoints, imageMatList.get(0).size(), cameraMatrix, distCoeffs, rvecs, tvecs, flagsCalib);
			
			Mat rotation = new Mat(3, 3, CvType.CV_64F);
			 for (int i = 0; i < rvecs.size(); i++) {
				 Calib3d.Rodrigues(rvecs.get(i), rotation);
				 MatOperations.LoggerHelper(rotation, "rot " + i);	
			 }*/
							
			double errReproj = MyCalibrateCamera.calibrateCamera(objectPoints, imagePoints, imageMatList, cameraMatrix, distCoeffs, rvecs, tvecs);
			System.out.println("Errep: " + errReproj);
			 /*for (int i = 0; i < rvecs.size(); i++) {				 
				 MatOperations.LoggerHelper(rvecs.get(i), "rot " + i);	
			 }*/
	}
	
	private static void getAllCornersFromImages(String path) {		
		
		File[] imagesList = new File(path).listFiles();
		for (File f : imagesList) {
			if (f.getName().contains(".png")) {
				
				Mat mat = Highgui.imread(f.getPath(), Highgui.CV_LOAD_IMAGE_COLOR);
				if (mat == null || mat.channels() != 3)
					continue;
				Mat gray = new Mat();
				Imgproc.cvtColor(mat, gray, Imgproc.COLOR_BGR2GRAY);
				MatOfPoint2f corners2f = new MatOfPoint2f();				
				if (!getChessBoardCornersFromOneImage(gray, corners2f)) {
					System.err.println("Ezen a képen nem találtam sakktáblát: " + f.getName());
					continue;
				}
				objectPoints.add(mCornersInChessSpace3f);
				/*System.out.println(corners2f.get(0, 0)[0] + " " + corners2f.get(0, 0)[1]);
				System.out.println(corners2f.get(1, 0)[0] + " " + corners2f.get(1, 0)[1]);
				System.out.println(corners2f.get(2, 0)[0] + " " + corners2f.get(2, 0)[1]);
				System.out.println(corners2f.get(3, 0)[0] + " " + corners2f.get(3, 0)[1]);*/
				imagePoints.add(corners2f);
				//imagePoints.add(normalizeBetween_minus1_1(corners2f));					
				imageMatList.add(mat);
				toFileString += "\n*" + f.getName() + "\nobjectPoints: \n "+ mCornersInChessSpace3f.dump();
				toFileString += "\nimagePoints: \n "+ corners2f.dump() + "\n";				
			}
		}			
	}
	
	private static MatOfPoint2f normalizeBetween_minus1_1(MatOfPoint2f corners2f) {
		Point[] vp = new Point[corners2f.rows()];
		MatOfPoint2f temPoint2f = new MatOfPoint2f();		
		for (int i = 0; i < corners2f.rows(); i++) {			
			vp[i] = new Point((2*corners2f.get(i, 0)[0] / 176)-1,	(2*corners2f.get(i, 0)[1] / 144)-1);			
		}
		temPoint2f.fromArray(vp);		
		
		return temPoint2f;
	}	
	
	private static boolean getChessBoardCornersFromOneImage(Mat gray, MatOfPoint2f corners2f) {
		if (!Calib3d.findChessboardCorners(gray, PATTERN_SIZE, corners2f, CALIB_PARAMS))
			return false;		
		return true;
	}
	
	//generate MatOfPoint3f from the points on the chessboard
	private static MatOfPoint3f getCorner3f() {
			MatOfPoint3f corner3f = new MatOfPoint3f();
			double squareSize = 1;		
			Point3[] vp = new Point3[(int) (PATTERN_SIZE.height * PATTERN_SIZE.width)];
			int cnt = 0;
			for (int i = 0; i < PATTERN_SIZE.height; ++i) {
				for (int j = 0; j < PATTERN_SIZE.width; ++j, cnt++) {
					vp[cnt] = new Point3(j * squareSize, i * squareSize, 0.0d);						
				}			
			}			
			corner3f.fromArray(vp);			
			return corner3f;
		}	

}
